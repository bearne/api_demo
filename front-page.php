<?php
/**
 * The template for displaying the API page
 * Template Name: API
 */
$server = $_SERVER['SERVER_NAME'];
if ( ! is_user_logged_in() && ! ( 0 < strpos( $server, '.dev' ) ) ) {
	wp_safe_redirect( '/' );
}
get_header();
?>

<section class="wrapper cf">
	<div class="endpoints">

		<div class="navigation">
			<p class="note">
				<strong>Note:</strong> For a full list of query parameters available to most endpoints, reference the <a
					href="http://v2.wp-api.org//">WP JSON API</a>.
			</p>
			<div class="toc">
				<h3>Table of Contents:</h3>
				<ul>
					<li><a href="#hit-endpoint-books">books</a></li>
					<li><a href="#hit-endpoint-book-id">books/id</a></li>
					<li><a href="#hit-endpoint-book-slug">books/slug</a></li>
					<li><a href="#hit-endpoint-book-isbn">books/isbn</a></li>
					<li><a href="#hit-endpoint-book-genre">books/genre/</a></li>
					<li><a href="#hit-endpoint-book-tags">books/tags/</a></li>
					<li><a href="#hit-endpoint-book-rating">books/rating/</a></li>
					<li><a href="#hit-endpoint-book-review-get">book review - Get</a></li>
					<li><a href="#hit-endpoint-book-review-create">book review - Create</a></li>

					<li><a href="#hit-endpoint-login">Login</a></li>
					<li><a href="#hit-endpoint-logout">Logout</a></li>
					<li><a href="#hit-endpoint-user">User</a></li>
					<li><a href="#hit-endpoint-user-update">User Update</a></li>
					<li><a href="#hit-endpoint-user-forgot">Forgot Password</a></li>
					<li><a href="#hit-endpoint-user-password">Reset Password</a></li>


					<li><a href="#hit-endpoint-contact">Contact</a></li>
				</ul>
			</div>
		</div>

		<section id="endpoint-books" class="endpoint status-ready">
			<h3>Books</h3>
			<span>GET</span>
			<code>/books/</code>

			<div class="example normal">
				<form>
					<button class="test-endpoint">Test Books</button>
				</form>
			</div>
		</section>

		<section id="endpoint-book-id" class="endpoint status-ready">
			<h3>Book by ID</h3>
			<span>GET</span>
			<code>/books/{id}/</code>

			<div class="example normal">
				<form>
					<form>
						<p><label>Id:</label><input type="text" name="id"></p>
						<button class="test-endpoint">Test  book by ID</button>
					</form>
			</div>
		</section>
		<section id="endpoint-book-slug" class="endpoint status-ready">
			<h3>Book by Slug</h3>
			<span>GET</span>
			<code>/books/{slug}/</code>

			<div class="example normal">
				<form>
					<form>
						<p><label>Slug:</label><input type="text" name="slug"></p>
						<button class="test-endpoint">Test book by slug</button>
					</form>
			</div>
		</section>
		<section id="endpoint-book-isbn" class="endpoint status-ready">
			<h3>Book by isbn</h3>
			<span>GET</span>
			<code>/books/{isbn}/</code>

			<div class="example normal">
				<form>
					<form>
						<p><label>ISBN:</label><input type="text" name="isbn"></p>
						<button class="test-endpoint">Test book by ISBN</button>
					</form>
			</div>
		</section>

		<section id="endpoint-book-genre" class="endpoint status-ready">
			<h3>Books by Genre</h3>
			<span>GET</span>
			<code>/books/genre/{genre}</code>

			<div class="example normal">
				<form>
					<form>
						<p><label>Genre:</label><input type="text" name="genre"></p>
						<button class="test-endpoint">Test book by Genre</button>
					</form>
			</div>
		</section>

		<section id="endpoint-book-rating" class="endpoint status-ready">
			<h3>Books by Rating</h3>
			<span>GET</span>
			<code>/books/rating/{rating}</code>

			<div class="example normal">
				<form>
					<form>
						<p><label>Rating:</label><input type="text" name="rating"></p>
						<button class="test-endpoint">Test book by Rating</button>
					</form>
			</div>
		</section>

		<section id="endpoint-book-rating-more" class="endpoint status-ready">
			<h3>Books by Rating and Compare</h3>
			<span>GET</span>
			<code>/books/rating/{value}/{less|more|just}</code>

			<div class="example normal">
				<form>
					<form><label>Compare:</label>
						<div>
							<label for="more">more:<input type="radio" id='more' type="text" name="less|more|just" value="more"></label>
							<label for="less">less:<input type="radio" id='less' type="text" name="less|more|just" value="less"></label>
							<label for="just">just:<input type="radio" id='just' type="text" name="less|more|just" value="just"></label>
						</div>
						<p><label>Rating:</label><input type="text" name="value"></p>
						<button class="test-endpoint">Test book by Rating</button>
					</form>
			</div>
		</section>

		<section id="endpoint-book-review-get" class="endpoint status-ready">
			<h3>Get revew</h3>
			<span>GET</span>
			<code>/books/{id}/reviews</code>

			<div class="example normal">
				<form>
					<form>
						<p><label>id:</label><input type="text" name="id"></p>
						<button class="test-endpoint">Get review</button>
					</form>
			</div>
		</section>

		<section id="endpoint-book-review-create" class="endpoint status-ready">
			<h3>Create revew</h3>
			<span>Post</span>
			<code>/books/{id}/reviews</code>
			<pre>
			{
			  "rating": "{rating}",
			  "review": "{review}"
			}
			</pre>
			<div class="example normal">
				<form>
					<form>
						<p><label>id:</label><input type="text" name="id"></p>
						<label>Rating:</label>
						<div>
							<label for="1">1:<input type="radio" id='1' type="text" name="rating" value="1"></label>
							<label for="2">2:<input type="radio" id='2' type="text" name="rating" value="2"></label>
							<label for="3">3:<input type="radio" id='3' type="text" name="rating" value="3"></label>
							<label for="4">4:<input type="radio" id='4' type="text" name="rating" value="4"></label>
							<label for="5">5:<input type="radio" id='5' type="text" name="rating" value="5"></label>
						</div>

						<p><label>Review:</label><textarea name="review"></textarea></p>
						<button class="test-endpoint">Create review</button>
					</form>
			</div>
		</section>


		<section id="endpoint-book-tags" class="endpoint status-ready">
			<h3>Books by Tags</h3>
			<span>GET</span>
			<code>/books/tags/{tag}</code>

			<div class="example normal">
				<form>
					<form>
						<p><label>Genre:</label><input type="text" name="tag"></p>
						<span class="note">as tag/tag/tag</span>
						<button class="test-endpoint">Test book by Tags</button>
					</form>
			</div>
		</section>


		<section id="endpoint-register" class="endpoint status-ready">
			<h3>Register - Normal</h3>
			<span>POST</span>
			<code>/register/</code>
<pre>
{
  "email": "{email}",
  "password": "{password}",
  "first_name": "{first_name}",
  "last_name": "{last_name}",
  "title": "{title}",

}
</pre>
			<div class="example normal">
				<form>
					<p class="required"><label>Email:</label><input type="text" name="email"></p>
					<p class="required"><label>Password:</label><input type="text" name="password"></p>
					<p class="required"><label>First Name:</label><input type="text" name="first_name"></p>
					<p class="required"><label>Last Name:</label><input type="text" name="last_name"></p>
					<p><label>Title:</label><input type="text" name="title"></p>

					<button class="test-email-registration">Test Email Registration</button>
				</form>
			</div>
		</section>

		<section id="endpoint-login" class="endpoint status-ready">
			<h3>Login - Normal</h3>
			<span>POST</span>
			<code>/login/</code>
<pre>
{
  "email": "{email}",
  "password": "{password}"
}
</pre>
			<div class="example">
				<form>
					<p class="required"><label>Email:</label><input type="text" name="email"></p>
					<p class="required"><label>Password:</label><input type="text" name="password"></p>
					<button class="test-login">Test Login</button>
				</form>
			</div>
		</section>

		<section id="endpoint-logout" class="endpoint status-ready">
			<h3>Logout</h3>
			<span>GET</span>
			<code>/logout/</code>
<pre>
{ }
</pre>
			<button class="test-endpoint">Test Logout</button>
		</section>

		<section id="endpoint-user" class="endpoint status-ready">
			<h3>User</h3>
			<span>POST</span>
			<code>/user/</code>
<pre>
{ }
</pre>
			<button class="test-endpoint">Test User</button>
		</section>

		<section id="endpoint-user-update" class="endpoint status-ready">
			<h3>User - Update</h3>
			<span>POST</span>
			<code>/user/update</code>
<pre>
{
  "email": "{email}",
  "password": "{password}",
  "first_name": "{first_name}",
  "last_name": "{last_name}",
  "title": "{title}",
}
</pre>
			<div class="example">
				<form>
					<p class="required"><label>Email:</label><input type="text" name="email"
					                                                value="<?php
					                                                esc_html_e( ( isset( $brc_user['email'] ) ) ? esc_attr( trim( $brc_user['email'] ) ) : '' ); ?>
					                                                "></p>
					<p><label>Password:</label><input type="text" name="password"></p>
					<p><label>First Name:</label><input type="text" name="first_name"
					                                    value="<?php esc_html_e( ( isset( $brc_user['first_name'] ) ) ? esc_attr( trim( $brc_user['first_name'] ) ) : '' ); ?>">
					</p>
					<p><label>Last Name:</label><input type="text" name="last_name"
					                                   value="<?php esc_html_e( ( isset( $brc_user['last_name'] ) ) ? esc_attr( trim( $brc_user['last_name'] ) ) : '' ); ?>">
					</p>
					<p><label>Title:</label><input type="text" name="title"
					                               value="<?php esc_html_e( ( isset( $brc_user['title'] ) ) ? esc_attr( trim( $brc_user['title'] ) ) : '' ); ?>">
					</p>
					<button class="test-update-user">Test Update User</button>
				</form>
			</div>
		</section>

		<section id="endpoint-user-forgot" class="endpoint status-ready">
			<h3>User - Forgot Password</h3>
			<span>POST</span>
			<code>/user/forgot-password/</code>
<pre>
{
  "email": "{email}",

}
</pre>
			<div class="example">
				<form>
					<p class="required"><label>Email:</label><input type="text" name="email" value="<?php
						esc_html_e( ( isset( $brc_user['email'] ) ) ? $brc_user['email'] : '' ); ?>"></p>
					<button class="test-endpoint">Test Forgot Password</button>
				</form>
			</div>
		</section>

		<section id="endpoint-user-password" class="endpoint status-ready">
			<h3>Users - Reset Password</h3>
			<span>POST</span>
			<code>/user/reset-password/</code>
<pre>
{
  "token": "{token}",
  "email": "{email}",
  "password": "{password}"
}
</pre>
			<div class="example">
				<form>
					<p class="required"><label>Token:</label><input type="text" name="token"></p>
					<p class="required"><label>Email:</label><input type="text" name="email"></p>
					<p class="required"><label>New Password:</label><input type="text" name="password"></p>
					<button class="test-endpoint">Test Reset Password</button>
				</form>
			</div>
		</section>


		<section id="endpoint-contact" class="endpoint status-ready">
			<h3>Contact</h3>
			<span>POST</span>
			<code>/contact/</code>
<pre>
{
  "name": "{name}",
  "email": "{email}",
  "subject": "{subject}",
  "body": "{body}",
}
</pre>
			<div class="example">
				<form>
					<p class="required"><label>Name:</label><input type="text" name="name"></p>
					<p class="required"><label>Email:</label><input type="text" name="email"></p>
					<p class="required"><label>Subject:</label><input type="text" name="subject"></p>
					<p class="required"><label>Body:</label><input type="text" name="body"></p>
					<div id="brc_recaptcha4" class="g-recaptcha"></div>
					<button class="test-update-user">Test Contact</button>
				</form>
			</div>
		</section>

	</div>

	<div class="result">
		<div class="inner">

			<h2>Endpoint:</h2>
			<span></span>
			<input type="text" name="url" placeholder="/pages/">
			<h2>Request:</h2>
			<textarea name="data" placeholder="{ posts_per_page: 100 }"></textarea>
<!--			<div class="options cf">-->
<!--				<button class="clear">Clear</button>-->
<!--				<button class="submit">Submit</button>-->
<!--			</div>-->
			<h2>Response:</h2>
			<pre></pre>
		</div>
	</div>

</section>


<?php get_footer(); ?>
