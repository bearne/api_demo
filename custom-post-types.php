<?php

/**
 * Register a $post_name post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
class API_Post_Type {

	public $post_type_name;
	public $post_type_args;
	public $post_type_labels;

	/**
	 * api_Post_Type constructor.
	 *
	 * @param $name
	 * @param array $args
	 * @param array $labels
	 */
	public function __construct( $name, $args = array(), $labels = array() ) {

		$this->post_type_name   = strtolower( str_replace( ' ', '_', $name ) );
		$this->post_type_args   = $args;
		$this->post_type_labels = $labels;

		if ( ! post_type_exists( $this->post_type_name ) ) {
			add_action( 'init', array( $this, 'api_register_post_type' ) );
		}
	}

	// Method which registers the post type
	public function api_register_post_type() {

		$name = ucwords( str_replace( '_', ' ', $this->post_type_name ) );

		$plural = self::pluralize( $name );

		$default_labels = array(
			'name'               => _x( $plural, 'post type general name', 'api-book' ),
			'singular_name'      => _x( $name, 'post type singular name', 'api-book' ),
			'add_new'            => _x( 'Add New', strtolower( $name ), 'api-book' ),
			'add_new_item'       => __( 'Add New ' . $name, 'api-book' ),
			'edit_item'          => __( 'Edit ' . $name, 'api-book' ),
			'new_item'           => __( 'New ' . $name, 'api-book' ),
			'all_items'          => __( 'All ' . $plural, 'api-book' ),
			'view_item'          => __( 'View ' . $name, 'api-book' ),
			'search_items'       => __( 'Search ' . $plural, 'api-book' ),
			'not_found'          => __( 'No ' . strtolower( $plural ) . ' found', 'api-book' ),
			'not_found_in_trash' => __( 'No ' . strtolower( $plural ) . ' found in Trash', 'api-book' ),
			'parent_item_colon'  => '',
			'menu_name'          => $plural,
		);

		$labels = array_merge( $default_labels, $this->post_type_labels );

		$default_args = array(
			'label'             => $plural,
			'labels'            => $labels,
			'public'            => true,
			'show_ui'           => true,
			'rewrite'           => array( 'slug' => strtolower( $plural ) ),
			'supports'          => array( 'title', 'editor', 'revisions' ),
			'show_in_nav_menus' => true,
			'map_meta_cap'      => true,
		);
		$args         = array_merge( $default_args, $this->post_type_args );

		register_post_type( $this->post_type_name, $args );

	}

	/**
	 * @param $name
	 * @param array $args
	 * @param array $labels
	 */
	public function api_add_taxonomy( $name, $args = array(), $labels = array() ) {

		if ( ! empty( $name ) ) {

			$post_type_name  = $this->post_type_name;
			$taxonomy_name   = strtolower( str_replace( ' ', '_', $name ) );
			$taxonomy_labels = $labels;
			$taxonomy_args   = $args;

			if ( ! taxonomy_exists( $taxonomy_name ) ) {

				$name   = ucwords( str_replace( '_', ' ', $name ) );
				$plural = self::pluralize( $name );

				$default_labels = array(
					'name'              => _x( $plural, 'taxonomy general name', 'api-book' ),
					'singular_name'     => _x( $name, 'taxonomy singular name' ),
					'search_items'      => __( 'Search ' . $plural , 'api-book' ),
					'all_items'         => __( 'All ' . $plural, 'api-book' ),
					'parent_item'       => __( 'Parent ' . $name, 'api-book' ),
					'parent_item_colon' => __( 'Parent ' . $name . ':', 'api-book' ),
					'edit_item'         => __( 'Edit ' . $name, 'api-book' ),
					'update_item'       => __( 'Update ' . $name, 'api-book' ),
					'add_new_item'      => __( 'Add New ' . $name, 'api-book' ),
					'new_item_name'     => __( 'New ' . $name . ' Name', 'api-book' ),
					'menu_name'         => __( $name ),
				);
				$labels         = array_merge( $default_labels, $taxonomy_labels );

				$default_args = array(
					'label'             => $plural,
					'labels'            => $labels,
					'public'            => true,
					'show_ui'           => true,
					'show_in_nav_menus' => true,
					'_builtin'          => false,
					'hierarchical'      => true,
				);
				$args         = array_merge( $default_args, $taxonomy_args );

				add_action( 'init', function () use ( $taxonomy_name, $post_type_name, $args ) {
					register_taxonomy( $taxonomy_name, $post_type_name, $args );
				} );

			} else {

				add_action( 'init', function () use ( $taxonomy_name, $post_type_name ) {
					register_taxonomy_for_object_type( $taxonomy_name, $post_type_name );
				} );

			}
		}
	}

	/**
	 * @param $name
	 *
	 * @return string
	 */
	public static function pluralize( $name ) {
		$last = $name[ strlen( $name ) - 1 ];
		$cut  = substr( $name, 0, - 1 );
		if ( 'y' == $last ) {
			$plural = $cut . 'ies';
		} elseif ( 's' == $last ) {
			$plural = $cut . 'es';
		} else {
			$plural = $name . 's';
		}

		return $plural;
	}
}



$book = new api_Post_Type( 'Book',
	array(
	'menu_icon' => 'dashicons-book-alt',
	)
);
$rewrite_genre_labels = array(
	'menu_name'     => 'Genre',
	'name'          => _x( 'Genre', 'taxonomy general name', 'api-book' ),
	'all_items'     => __( 'All Genres', 'api-book' ),
	'singular_name' => 'Genre',
	'add_new_item'  => __( 'Add New Genre' , 'api-book' ),
	'search_items'  => __( 'Search Genres', 'api-book' ),
);
$book->api_add_taxonomy( 'Genre', array(), $rewrite_genre_labels );



$author = new api_Post_Type( 'Author', array(
		'menu_icon' => 'dashicons-businessman',
	)
);

$rewrite_tag_args = array( 'hierarchical' => false );
$rewrite_tag_labels = array(
	'menu_name'     => 'Tags',
	'name'          => _x( 'Tags', 'taxonomy general name', 'api-book' ),
	'all_items'     => __( 'All Tags', 'api-book' ),
	'singular_name' => 'Tag',
	'add_new_item'  => __( 'Add New Tag' , 'api-book' ),
	'search_items'  => __( 'Search Tags', 'api-book' ),
);

$book->api_add_taxonomy( 'Book Tags', $rewrite_tag_args, $rewrite_tag_labels );
$author->api_add_taxonomy( 'Author Tags', $rewrite_tag_args, $rewrite_tag_labels );


