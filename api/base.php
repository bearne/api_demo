<?php

//- ------------------------------
//- Base API Class
//- ------------------------------

/**
 * Class BRC_Base_API
 */
class Base_API {

	/**
	 * Get a list of items
	 *
	 * @param array $options
	 *
	 * @return WP_JSON_Response
	 */
	public static function get_items( $options = array() ) {

		$default_options = array(
			'type'              => 'page',
			'args'              => array(),
			'fields'            => array(),
			'fields_transforms' => array(),
			'terms'             => array(),
			'terms_transforms'  => array(),
			'hidden_fields'     => array(),
			'translated'        => true,
		);

		$options = array_merge( $default_options, $options );

		$options['type'] = ( 'search' === $options['type'] ) ? '' : $options['type'];

		$default_args = array(
			'posts_per_page' => 50,
			'offset'         => 0,
			'orderby'        => 'date',
			'order'          => 'DESC',
			'meta_key'       => '',
			'meta_value'     => '',
			's'              => '',
		);

		$strict_args = array(
			'category'         => '',
			'category_name'    => '',
			'include'          => '',
			'exclude'          => '',
			'post_type'        => $options['type'],
			'post_mime_type'   => '',
			'post_parent'      => '',
			'post_status'      => 'publish',
			'suppress_filters' => true,
		);

		$options['args'] = array_merge( $default_args, $options['args'] );
		$options['args'] = array_merge( $options['args'], $strict_args );

		// Create post query and JSON response headers

		$query    = new WP_Query( $options['args'] );
		$response = ( defined( 'JSON_API_VERSION' ) ) ? new WP_JSON_Response(): new WP_REST_Response;
		if ( defined( 'JSON_API_VERSION' ) ) {
			// included by default
			$response->query_navigation_headers( $query );
		}

		$response->header( 'Last-Modified', mysql2date( 'D, d M Y H:i:s', get_lastpostmodified( 'GMT' ), 0 ) . ' GMT' );
		// as we change content in user is logged in we not to cache this
		$response->header( 'Cache-Control', 'no-cache, must-revalidate, max-age=0', true );

		// Setup paging information
		$data = array(
			'found_items'   => absint( $query->found_posts ),
			'paged'         => absint( $query->get( 'paged' ) ),
			'max_num_items' => absint( $query->max_num_pages ),
			'items'         => array(),
		);

		// Check if posts exist
		if ( ! $query ) {
			$response->set_status( 400 );

			return $response;
		}

		// Iterate over posts, get the translation and normalize the data
		foreach ( $query->posts as $post ) {
			// Get translated content
			if ( true === $options['translated'] ) {
				$post = get_post( $post->ID );
			}

			if ( $post ) {
				// Normalize the post data
				$post = self::normalize_post_data( $post, $options );

				// Set the response data
				$data['items'][] = $post;
			} else {
				$data['items'][] = self::get_edit_post_link( $post->ID );
			}
		}

		// Set the response data
		$response->set_data( $data );

		return $response;
	}

	/**
	 * Get a single item
	 *
	 * @param array $options
	 *
	 * @return WP_JSON_Response
	 */
	public static function get_item( $options = array() ) {

		$default_options = array(
			'id'                => 1,
			'fields'            => array(),
			'fields_transforms' => array(),
			'terms'             => array(),
			'terms_transforms'  => array(),
			'hidden_fields'     => array(),
			'translated'        => true,
		);

		$options = array_merge( $default_options, $options );

		// Create post query and JSON response headers
		$response = ( defined( 'JSON_API_VERSION' ) ) ? new WP_JSON_Response(): new WP_REST_Response;
		$response->header( 'Last-Modified', mysql2date( 'D, d M Y H:i:s', get_lastpostmodified( 'GMT' ), 0 ) . ' GMT' );

		// Check if the id passed is even the proper post type
		if ( get_post_type( get_post( $options['id'] ) ) !== $options['type'] ) {
			$response->set_status( 400 );

			return $response;
		}

		$post = get_post( $options['id'] );

		// Check if post exists
		if ( ! $post ) {
			$response->set_status( 400 );
			$response->set_data( self::get_edit_post_link( $options['id'] ) );
			return $response;
		} else {
			// Normalize the post data
			$post = self::normalize_post_data( $post, $options );

			// Set the response data
			$response->set_data( $post );
		}

		return $response;
	}

	/**
	 * Normalize Post Data
	 *
	 * @param $post
	 * @param $options
	 *
	 * @return mixed
	 */
	public static function normalize_post_data( $post, $options ) {

		$hidden_fields = array(
			'post_excerpt',
			'post_status',
			'comment_status',
			'post_password',
			'ping_status',
			'to_ping',
			'pinged',
			'post_content_filtered',
			'post_parent',
			'comment_count',
			'filter',
			'post_mime_type',
			'post_date_gmt',
			'post_modified_gmt',
		);

		$hidden_fields = array_merge( $hidden_fields, $options['hidden_fields'] );

		$hidden_terms = array(
			'term_id',
			'term_taxonomy_id',
			'term_group',
			'taxonomy',
			'parent',
			'count',
			'filter',
			'object_id',
		);

		// Iterate and add custom fields
		foreach ( $options['fields'] as $field ) {

			$post->{$field} = get_post_meta( $post->ID, $field, true );

			// Do a custom field transformation if one exists
			if ( isset( $options['fields_transforms'][ $field ] ) && is_callable( $options['fields_transforms'][ $field ] ) ) {
				$post->{$field} = $options['fields_transforms'][ $field ]( $post->{$field}, $post->ID );
			}
		}

		// Iterate and add custom terms
		foreach ( $options['terms'] as $term ) {

			$post->{$term} = get_the_terms( $post->ID, $term );

			// Do a custom terms transformation if one exists
			if ( isset( $options['terms_transforms'][ $term ] ) && is_callable( $options['terms_transforms'][ $term ] ) ) {
				$post->{$term} = $options['terms_transforms'][ $term ]( $post->{$term}, $post->ID );
			}

			// Remove irrelevant terms from response
			foreach ( $hidden_terms as $key ) {
				unset( $post->{$term}->{$key} );
			}
		}

		// Remove irrelevant fields rom response
		foreach ( $hidden_fields as $field ) {
			unset( $post->{$field} );
		}

		return $post;
	}

	/**
	 * @param $image
	 *
	 * @return array
	 */
	public static function get_retina_image_set( $image ) {

		$type = wp_check_filetype( $image );
		if ( 'svg' !== $type['ext'] ) {
			$retina     = wp_get_attachment_image_src( $image, 'full' );
			$width      = $retina[1] / 2;
			$height     = $retina[2] / 2;
			$normal     = wp_get_attachment_image_src( $image, array( $width, $height ) );
			$normal_src = $normal[0];
			$retina_src = $retina[0];
		} else {
			$normal_src = $image;
			$retina_src = $image;
		}

		return array(
			'normal' => api_make_link_relative_if_local( $normal_src ),
			'retina' => api_make_link_relative_if_local( $retina_src ),
		);
	}

	/**
	 * Retrieve edit posts link for post.
	 *
	 * Can be used within the WordPress loop or outside of it. Can be used with
	 * pages, posts, attachments, and revisions.
	 *
	 * @since 2.3.0
	 *
	 * @param int    $id      Optional. Post ID.
	 * @param string $context Optional, defaults to display. How to write the '&', defaults to '&amp;'.
	 * @return string The edit post link for the given post. null if the post type is invalid or does
	 *                     not allow an editing UI.
	 */
	private static function get_edit_post_link( $id = 0, $context = 'display' ) {

		if ( ! $post = get_post( $id ) ) {

			return '';
		}

		if ( 'revision' === $post->post_type ) {
			$action = '';
		} elseif ( 'display' == $context ) {
			$action = '&amp;action=edit';
		} else {
			$action = '&action=edit';
		}
		$post_type_object = get_post_type_object( $post->post_type );

		if ( ! $post_type_object ) {

			return '';
		}

		if ( $post_type_object->_edit_link ) {
			$link = admin_url( sprintf( $post_type_object->_edit_link . $action, $post->ID ) );
		} else {
			$link = '';
		}

		/**
		 * Filter the post edit link.
		 *
		 * @since 2.3.0
		 *
		 * @param string $link    The edit link.
		 * @param int    $post_id Post ID.
		 * @param string $context The link context. If set to 'display' then ampersands
		 *                        are encoded.
		 */
		return apply_filters( 'get_edit_post_link', $link, $post->ID, $context );
	}
}
