<?php

/**
 * Class BRC_Contact
 */
class Api_Books extends Base_API {

	// the post type
	private static $post_type = 'book';

	/**
	 * @param $routes
	 *
	 * @return mixed
	 */
	public function register_routes_v1( $routes ) {

		$routes['/books/genre/(?P<id>.+)'] = array(
			array( array( __CLASS__, 'get_books_by_genre' ), WP_JSON_Server::READABLE | WP_JSON_Server::ACCEPT_JSON ),
		);

		$routes['/books/tags/(?P<id>.+)'] = array(
			array( array( __CLASS__, 'get_books_by_tags' ), WP_JSON_Server::READABLE ),
		);

		$routes['/books/rating/(?P<value>.+)/(?P<compare>.+)'] = array(
			array( array( __CLASS__, 'get_books_by_rating' ), WP_JSON_Server::READABLE ),
		);

		$routes['/books/rating/(?P<value>.+)'] = array(
			array( array( __CLASS__, 'get_books_by_rating' ), WP_JSON_Server::READABLE ),
		);

		$routes['/books/(?P<id>.+)'] = array(
			array( array( __CLASS__, 'get_books' ), WP_JSON_Server::READABLE | WP_JSON_Server::ACCEPT_JSON ),
		);

		$routes['/books'] = array(
			array( array( __CLASS__, 'get_books' ), WP_JSON_Server::READABLE | WP_JSON_Server::ACCEPT_JSON ),
		);

		return $routes;
	}

	public function register_routes_v2() {

		register_rest_route( 'api-demo/v1', '/books/genre/(?P<id>.+)', array(
			'callback' => array( __CLASS__, 'get_books_by_genre' ),
			'methods'  => WP_REST_Server::READABLE,
		) );
		register_rest_route( 'api-demo/v1', '/books/tags/(?P<id>.+)', array(
			'callback' => array( __CLASS__, 'get_books_by_tags' ),
			'methods'  => WP_REST_Server::READABLE,
		) );

		register_rest_route( 'api-demo/v1', '/books/rating/(?P<value>.+)/(?P<compare>.+)', array(
			'callback' => array( __CLASS__, 'get_books_by_rating' ),
			'methods'  => WP_REST_Server::READABLE,
		) );

		register_rest_route( 'api-demo/v1', '/books/rating/(?P<value>.+)', array(
			'callback' => array( __CLASS__, 'get_books_by_rating' ),
			'methods'  => WP_REST_Server::READABLE,
		) );

		register_rest_route( 'api-demo/v1', '/books/(?P<id>.+)/reviews', array(
			'callback' => array( __CLASS__, 'get_reviews' ),
			'methods'  => WP_REST_Server::READABLE,
		) );

		register_rest_route( 'api-demo/v1', '/books/(?P<id>.+)/reviews', array(
			'callback' => array( __CLASS__, 'create_review' ),
			'methods'  => WP_REST_Server::EDITABLE,
		) );
		register_rest_route( 'api-demo/v1', '/books/(?P<id>.+)', array(
			'callback' => array( __CLASS__, 'get_books' ),
			'methods'  => WP_REST_Server::READABLE,
		) );

		register_rest_route( 'api-demo/v1', '/books', array(
			'callback' => array( __CLASS__, 'get_books' ),
			'methods'  => WP_REST_Server::READABLE,
		) );
	}


	/**
	 * @param WP_REST_Request | array $args
	 * @param null $id
	 *
	 * @return WP_JSON_Response
	 * @internal param $data
	 *
	 */
	public static function get_books( $args = array(), $id = null ) {

		$response = ( defined( 'JSON_API_VERSION' ) ) ? new WP_JSON_Response() : new WP_REST_Response;

		if ( is_object( $args ) ) {
			$params = $args->get_params();
			$args   = array();
			$id     = ( isset( $params['id'] ) ) ? $params['id'] : null;
		}
		$fields            = array( 'reviews', 'cover_image', 'av_rating', 'isbn' );
		$terms             = array( 'genre', 'book_tags' );
		$hidden_fields     = array(
			'post_date',
			'post_modified',
			'guid',
			'post_type',
			'post_author',
		);
		$fields_transforms = array(
			'cover_image' => function ( $field ) {
				$fields['cover_image'] = self::get_retina_image_set( $field );

				return $fields;
			},
			'name'        => function ( $fields, $id ) {
				$title = get_the_title( $id );
				if ( ! empty( $title ) && 'Auto Draft' !== $title ) {
					return $title;
				}

				return $fields;
			},
		);
		$terms_transforms  = array(
			'genre'     => function ( $terms ) {
				if ( isset( $terms[0] ) ) {
					return array( $terms[0]->slug => $terms[0]->name );
				}

				return '';
			},
			'book_tags' => function ( $terms ) {
				if ( is_array( $terms ) ) {
					foreach ( $terms as $key => $val ) {
						$terms[ $key ] = array( $val->slug => $val->name );
					}
				}

				return $terms;
			},
		);

		// no id value - get all books
		if ( null === $id ) {

			$args = array_merge( $args, array(
				'posts_per_page' => 500,
				'orderby'        => 'menu_order, post_title',
				'order'          => 'asc',
			) );

			$options = array(
				'type'              => self::$post_type,
				'args'              => $args,
				'fields'            => $fields,
				'terms'             => $terms,
				'hidden_fields'     => $hidden_fields,
				'fields_transforms' => $fields_transforms,
				'terms_transforms'  => $terms_transforms,
			);

			return self::get_items( $options );
		} else {

			// if we are have an isbn to an meta query to book ath match
			if ( false !== self::is_isbn( $id ) ) {
				$args     = array(
					'post_type'      => self::$post_type,
					'post_status'    => 'publish',
					'posts_per_page' => 1,
					'meta_query'     => array(
						array(
							'key'     => 'isbn',
							'value'   => $id,
							'compare' => '=',
						),
					),
				);
				$my_posts = get_posts( $args );
				if ( $my_posts ) {
					$id = $my_posts[0]->ID;
				}
			}
			// if we have string look it up
			if ( ! is_numeric( $id ) ) {
				$args = array(
					'name'           => $id,
					'post_type'      => self::$post_type,
					'post_status'    => 'publish',
					'posts_per_page' => 1,
				);

				$my_posts = get_posts( $args );
				if ( $my_posts ) {
					$id = $my_posts[0]->ID;
				}
			}

			if ( ! is_numeric( $id ) ) {

				$response->set_status( 401 );

				$response->set_data( array( 'found_items' => 0, 'message' => __( 'ID not found', 'api-book' ) ) );
				return $response;
			}

			$options = array(
				'type'              => self::$post_type,
				'id'                => $id,
				'fields'            => $fields,
				'terms'             => $terms,
				'hidden_fields'     => $hidden_fields,
				'fields_transforms' => $fields_transforms,
				'terms_transforms'  => $terms_transforms,
			);

			return self::get_item( $options );
		}
	}

	/**
	 * create the tax_query for a genre query
	 *
	 * @static
	 *
	 * @param WP_REST_Request | array | null $data
	 * @param $id
	 *
	 * @return WP_JSON_Response
	 */
	public static function get_books_by_genre( $data = null, $id = null ) {
		if ( is_object( $data ) ) {
			$params = $data->get_params();
			$data   = null;
			$id     = ( isset( $params['id'] ) ) ? $params['id'] : null;
		}
		if ( null === $id ) {
			$response = ( defined( 'JSON_API_VERSION' ) ) ? new WP_JSON_Response() : new WP_REST_Response;
			$response->set_status( 400 );
			$response->set_data( array( 'message' => 'genre missing' ) );

			return $response;
		}
		$args = array(
			'tax_query' => array(
				array(
					'taxonomy' => 'genre',
					'field'    => 'slug',
					'terms'    => $id,
				),
			),
		);

		return self::get_books( $args );
	}

	/**
	 * create the tax_query for a genre query
	 *
	 * @static
	 *
	 * @param WP_REST_Request | array | null $data
	 * @param $id
	 *
	 * @return WP_JSON_Response
	 */
	public static function get_books_by_tags( $data = null, $id = null ) {
		if ( is_object( $data ) ) {
			$params = $data->get_params();
			$data   = null;
			$id     = ( isset( $params['id'] ) ) ? $params['id'] : null;
		}

		// the ID contains the URL after the tags
		$tags = explode( '/', $id );

		$args = array(
			'tax_query' => array(
				array(
					'taxonomy' => 'book_tags',
					'field'    => 'slug',
					'terms'    => $tags,
				),
			),
		);

		return self::get_books( $args );
	}


	/**
	 * create the tax_query for a genre query
	 *
	 * @static
	 *
	 * @param WP_REST_Request | array | null $data
	 * @param null $value
	 *
	 * @return WP_JSON_Response
	 * @internal param null $type
	 *
	 */
	public static function get_books_by_rating( $data = null, $value = null ) {

		if ( is_object( $data ) ) {
			$params       = $data->get_params();
			$value        = ( isset( $params['value'] ) ) ? $params['value'] : null;
			$compare_type = ( isset( $params['compare'] ) ) ? $params['compare'] : null;
		} else {
			$compare_type = ( isset( $data['compare'] ) ) ? $data['compare'] : null;
		}

		if ( 0 >= $value || 5 <= $value ) {
			$response = ( defined( 'JSON_API_VERSION' ) ) ? new WP_JSON_Response() : new WP_REST_Response;
			$response->set_status( 401 );
			$response->set_data( array( 'message' => 'Value out of range' ) );

			return $response;
		}

		$compare = '=';
		switch ( $compare_type ) {
			case 'less':
				$compare = '<=';
				break;
			case 'more':
				$compare = '>=';
				break;
		}

		$args = array(
			'meta_key'     => 'av_rating',
			'meta_value'   => $value,
			'meta_compare' => $compare,
		);

		return self::get_books( $args );
	}

	/**
	 * Get reviews for book by ID
	 *
	 * @static
	 *
	 * @param WP_REST_Request | array | null $data
	 * @param null $id
	 *
	 * @return WP_JSON_Response
	 * @internal param null $value
	 * @internal param null $type
	 *
	 */
	public static function get_reviews( $data = null, $id = null ) {

		if ( is_object( $data ) ) {
			$params = $data->get_params();
			$data   = null;
			$id     = ( isset( $params['id'] ) ) ? $params['id'] : null;
		}

		$fields        = array( 'reviews', 'av_rating' );
		$hidden_fields = array(
			'post_date',
			'post_modified',
			'guid',
			'post_type',
			'post_author',
			'post_content',
			'post_title',
			'post_name',
			'menu_order',
		);
		$response      = ( defined( 'JSON_API_VERSION' ) ) ? new WP_JSON_Response() : new WP_REST_Response;
		// no id value - get all books
		if ( null !== $id ) {

			// if we have string look it up to get ID
			if ( ! is_numeric( $id ) ) {
				$args = array(
					'name'           => $id,
					'post_type'      => self::$post_type,
					'post_status'    => 'publish',
					'posts_per_page' => 1,
				);

				$my_posts = get_posts( $args );
				if ( $my_posts ) {
					$id = $my_posts[0]->ID;
				}
			}

			if ( ! is_numeric( $id ) ) {
				$response->set_status( 401 );

				$response->set_data( array( 'found_items' => 0, 'message' => __( 'ID not found', 'api-book' ) ) );
				return $response;
			}

			$options = array(
				'type'          => self::$post_type,
				'args'          => array( 'page_id' => $id ),
				'fields'        => $fields,
				'hidden_fields' => $hidden_fields,
			);

			return self::get_items( $options );
		} else {

			$response->set_status( 400 );

			$response->set_data( array( 'message' => 'Id missing' ) );
			return $response;
		}
	}

	/**
	 * Get reviews for book by ID
	 *
	 * @static
	 *
	 * @param WP_REST_Request | array | null $data
	 * @param null $id
	 *
	 * @return WP_JSON_Response
	 * @internal param null $value
	 * @internal param null $type
	 *
	 */
	public static function create_review( $data = null, $id = null ) {

		$response = ( defined( 'JSON_API_VERSION' ) ) ? new WP_JSON_Response() : new WP_REST_Response;

		if ( is_user_logged_in() && current_user_can( 'edit_posts' ) ) {
			$response->set_status( 400 );

			return $response;
		}


		if ( is_object( $data ) ) {
			$params = $data->get_params();
			$data   = null;
			$id     = ( isset( $params['id'] ) ) ? $params['id'] : null;
		}


		if ( null === $id ) {
			$response->set_status( 410 );

			$response->set_data( array( 'message' => 'bad ID' ) );
			return $response;
		}

		$rating = ( isset( $params['rating'] ) ) ? $params['rating'] : null;
		if ( null === $rating ) {
			$response->set_status( 411 );

			$response->set_data( array( 'message' => 'rating missing' ) );
			return $response;
		}

		$review = ( isset( $params['review'] ) ) ? $params['review'] : null;
		if ( null === $review ) {
			$response->set_status( 412 );

			$response->set_data( array( 'message' => 'review missing' ) );
			return $response;
		}

		$current_reviews = get_post_meta( $id, 'reviews', true );

		if ( false === $current_reviews ) {
			$current_reviews = array();
		}

		// add the new review
		$current_reviews[] = array( 'rating' => $rating, 'review' => $review );
		// resave the reviews1
		update_post_meta( $id, 'reviews', $current_reviews );
		// update the av_rating
		$av_rating = get_post_meta( $id, 'av_rating', true );

		$no_of_reviews = count( $current_reviews );

		// do so math to work the new av rating ot store :-)
		$new_av_rating = ( ( $av_rating * ( $no_of_reviews - 1 ) ) + $rating ) / $no_of_reviews;

		update_post_meta( $id, 'av_rating', $new_av_rating );

		return self::get_reviews( null, $id );
	}

	/**
	 * http://stackoverflow.com/questions/14095778/regex-differentiating-between-isbn-10-and-isbn-13
	 *
	 * @static
	 *
	 * @param $str
	 *
	 * @return bool|int
	 */
	private static function is_isbn( $str ) {
		$regex = '/\b(?:ISBN(?:: ?| ))?((?:97[89])?\d{9}[\dx])\b/i';

		if ( preg_match( $regex, str_replace( '-', '', $str ), $matches ) ) {
			return ( 10 === strlen( $matches[1] ) )
				? 1   // ISBN-10
				: 2;  // ISBN-13
		}

		return false; // No valid ISBN found
	}
}
