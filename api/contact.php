<?php
/**
 * Class BRC_Contact
 */
class Contact extends Base_API {

	/**
	 * @param $routes
	 *
	 * @return mixed
	 */
	public function register_routes_v1( $routes ) {

		$routes['/contact'] = array(
			array(
				array( $this, 'contact' ),
				WP_JSON_Server::READABLE | WP_JSON_Server::EDITABLE | WP_JSON_Server::ACCEPT_JSON,
			),
		);

		return $routes;

	}

	public function register_routes_v2( $routes ) {

		register_rest_route( 'api-demo/v1', '/contact', array(
			'callback' => array( __CLASS__, 'contact' ),
			'methods'  => WP_REST_Server::READABLE | WP_REST_Server::EDITABLE,
		) );
	}


	// Contact
	/**
	 * @param $data
	 *
	 * @return WP_JSON_Response
	 */
	public function contact( $data ) {

		$response = new WP_JSON_Response();

		// Sanitize input
		$name      = sanitize_text_field( $data['name'] );
		$email     = sanitize_text_field( $data['email'] );
		$subject   = sanitize_text_field( $data['subject'] );
		$body      = sanitize_text_field( $data['body'] );

		$errors = array();
		// Validate name
		if ( empty( $name ) || strlen( $name ) < 1 ) {
			$errors[] = 'name';
		}

		// Validate subject
		if ( empty( $subject ) || strlen( $subject ) < 1 ) {
			$errors[] = 'subject';
		}

		// Validate body
		if ( empty( $body ) || strlen( $body ) < 1 ) {
			$errors[] = 'body';
		}

		// Validate email
		if ( ! filter_var( $email, FILTER_VALIDATE_EMAIL ) || empty( $email ) || strlen( $email ) < 1 ) {
			$errors[] = 'email';
		}

		// Check for any validation errors
		if ( count( $errors ) >= 1 ) {

			$response->set_status( 400 );
			$response->set_data( $errors );

			return $response;
		}


		// Send email to FB
		$send = wp_mail(  'Website admin <' .get_option( 'admin_email' ) . '>', __( 'Contact Request:', 'api-demo' ) . ' ' . $subject . ' : ' . $name, $body );

		if( true === $send ){
			// Send auto Response to user
			$message = 'Thank you for you message. We will reply in 24hr';
			$send = wp_mail( $name . '<' . $email . '>', __( 'Your question was submitted', 'api-demo' ), $message );
		}

		if( true === $send ) {
			$response->set_status( 200 );
			$response->set_data( array( 'message' => 'success' ) );
		} else {
			$response->set_status( 400 );
			$response->set_data( array( 'message' => 'Failed to send email' ) );
		}


		return $response;
	}
}
