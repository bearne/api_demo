<?php

//- ------------------------------
//- Users API Class
//- ------------------------------

/**
 * Class BRC_Users
 */
class  Api_Users extends Base_API {

	/**
	 * @param $routes
	 *
	 * @return mixed
	 */
	public function register_routes_v1( $routes ) {

		$routes['/register']             = array(
			array( array( $this, 'register' ), WP_JSON_Server::CREATABLE | WP_JSON_Server::ACCEPT_JSON ),
		);
		$routes['/login']                = array(
			array( array( $this, 'login' ), WP_JSON_Server::EDITABLE | WP_JSON_Server::ACCEPT_JSON ),
		);
		$routes['/logout']               = array(
			array( array( $this, 'logout' ), WP_JSON_Server::READABLE | WP_JSON_Server::ACCEPT_JSON ),
		);
		$routes['/user']                 = array(
			array(
				array( $this, 'get_user' ),
				WP_JSON_Server::EDITABLE | WP_JSON_Server::READABLE | WP_JSON_Server::ACCEPT_JSON,
			),
		);
		$routes['/user/update']          = array(
			array( array( $this, 'edit_user' ), WP_JSON_Server::EDITABLE | WP_JSON_Server::ACCEPT_JSON ),
		);
		$routes['/user/forgot-password'] = array(
			array(
				array( $this, 'forgot_password' ),
				WP_JSON_Server::EDITABLE | WP_JSON_Server::READABLE | WP_JSON_Server::ACCEPT_JSON,
			),
		);
		$routes['/user/reset-password']  = array(
			array(
				array( $this, 'reset_password' ),
				WP_JSON_Server::EDITABLE | WP_JSON_Server::READABLE | WP_JSON_Server::ACCEPT_JSON,
			),
		);

		return $routes;
	}

	public function register_routes_v2() {

		register_rest_route( 'api-demo/v1', '/user/register', array(
			'callback' => array( __CLASS__, 'register' ),
			'methods'  => WP_REST_Server::CREATABLE ,
		) );
		register_rest_route( 'api-demo/v1', '/user/login', array(
			'callback' => array( __CLASS__, 'login' ),
			'methods'  => WP_REST_Server::EDITABLE | WP_REST_Server::READABLE,
		) );
		register_rest_route( 'api-demo/v1', '/user/logout', array(
			'callback' => array( __CLASS__, 'logout' ),
			'methods'  => WP_REST_Server::READABLE,
		) );

		register_rest_route( 'api-demo/v1', '/user', array(
			'callback' => array( __CLASS__, 'get_user' ),
			'methods'  => WP_REST_Server::READABLE,
		) );

		register_rest_route( 'api-demo/v1', '/user/update', array(
			'callback' => array( __CLASS__, 'edit_user' ),
			'methods'  => WP_REST_Server::EDITABLE | WP_REST_Server::READABLE,
		) );
		register_rest_route( 'api-demo/v1', '/user/forgot-password', array(
			'callback' => array( __CLASS__, 'get_user' ),
			'methods'  => WP_REST_Server::EDITABLE | WP_REST_Server::READABLE,
		) );

		register_rest_route( 'api-demo/v1', '/user/reset-password', array(
			'callback' => array( __CLASS__, 'reset_password' ),
			'methods'  => WP_REST_Server::EDITABLE | WP_REST_Server::READABLE ,
		) );

	}

	// Get user
	/**
	 * @return WP_JSON_Response
	 */
	public function get_user() {

		$response = ( defined( 'JSON_API_VERSION' ) ) ? new WP_JSON_Response(): new WP_REST_Response;

		// Get current user
		global $brc_user;

		// Authentic user
		if ( ! $this->is_user_logged_in() ) {
			$response->set_status( 401 );

			return $response;
		}

		$user = wp_get_current_user();
		$response->set_status( 200 );
		$response->set_data( array(
			'ID'         => $user->ID,
			'email'      => $user->user_email,
			'first_name' => get_user_meta( $user->ID, 'first_name', true ),
			'last_name'  => get_user_meta( $user->ID, 'last_name', true ),
			'title'      => get_user_meta( $user->ID, 'title', true ),
		) );

		return $response;

	}

	// Register user
	/**
	 * @param array $data
	 *
	 * @return WP_JSON_Response
	 */
	public function register( $data = array() ) {

		$response = ( defined( 'JSON_API_VERSION' ) ) ? new WP_JSON_Response(): new WP_REST_Response;
		$response->set_headers( array( 'Cache-Control' => 'no-cache, must-revalidate, max-age=0 ' ) );

		// Setup values
		$password   = ( isset( $data['password'] ) ) ? sanitize_text_field( $data['password'] ) : '';
		$email      = ( isset( $data['email'] ) ) ? sanitize_text_field( $data['email'] ) : '';
		$first_name = ( isset( $data['first_name'] ) ) ? sanitize_text_field( $data['first_name'] ) : '';
		$last_name  = ( isset( $data['last_name'] ) ) ? sanitize_text_field( $data['last_name'] ) : '';
		$title      = ( isset( $data['title'] ) ) ? sanitize_text_field( $data['title'] ) : '';

		// Setup validation errors array
		$errors = array();

		// Validate first name
		$errors = $this->validate_first_name( $first_name, $errors );

		// Validate last name
		$errors = $this->validate_last_name( $last_name, $errors );

		// vakiaate email
		$errors = $this->validate_email( $email, $errors );

		if ( ! empty( $email ) && email_exists( $email ) ) {

			$errors[] = 'email_exists';
		}

		// Validate password
		$errors = $this->validate_password( $password, $errors );

		// Check for any validation errors
		if ( count( $errors ) >= 1 ) {
			$response->set_status( 400 );
			$response->set_data( $errors );

			return $response;
		}

		// Check if user exists
		if ( email_exists( $email ) ) {
			$response->set_status( 409 );

			return $response;
		}


		// Create new user name
		$username = strtolower( $first_name . '.' . $last_name );
		$i        = 1;
		//loop and+1 untill new name found
		while ( username_exists( $username ) ) {
			$i ++;
			$username = strtolower( $first_name . '.' . $last_name . '.' . $i );
		}

		// Register new user
		$user_data = array(
			'user_login'           => $username,
			'user_email'           => $email,
			'user_pass'            => $password,
			'first_name'           => $first_name,
			'last_name'            => $last_name,
			'show_admin_bar_front' => false,
			'role'                 => 'book-reviewer',
		);

		// Insert user into database
		$user_id = wp_insert_user( $user_data );

		// Set user attributes
		update_user_meta( $user_id, 'first_name', $first_name );
		update_user_meta( $user_id, 'last_name', $last_name );
		update_user_meta( $user_id, 'title', $title );

		// Check for successful registration & login
		if ( $user_id ) {

			$user = get_user_by( 'id', $user_id );
			$this->set_authentication_cookies( $user );
			$cookie = $this->set_auth_cookie( $user );
			$response->set_status( 200 );
			$response->set_data( array(
				'ID'         => $user->ID,
				'email'      => $user->user_email,
				'first_name' => get_user_meta( $user->ID, 'first_name', true ),
				'last_name'  => get_user_meta( $user->ID, 'last_name', true ),
				'title'      => get_user_meta( $user->ID, 'title', true ),
			) );

			return $response;
		} else {

			$response->set_status( 500 );

			return $response;
		}

	}

	// Login user
	/**
	 * @param $data
	 *
	 * @return WP_JSON_Response
	 */
	public function login( $data ) {

		$response = ( defined( 'JSON_API_VERSION' ) ) ? new WP_JSON_Response(): new WP_REST_Response;
		$response->set_headers( array( 'Cache-Control' => 'no-cache, must-revalidate, max-age=0 ' ) );
		if ( isset( $data['password'] ) && isset( $data['email'] ) ) {
			$password = sanitize_text_field( $data['password'] );
			$email    = sanitize_text_field( $data['email'] );

			$user = get_user_by( 'email', $email );
			if ( $user && wp_check_password( $password, $user->data->user_pass, $user->ID ) ) {
				$this->set_authentication_cookies( $user );
				$cookie = $this->set_auth_cookie( $user );
				$response->set_data( array(
					'ID'         => $user->ID,
					'email'      => $user->user_email,
					'first_name' => get_user_meta( $user->ID, 'first_name', true ),
					'last_name'  => get_user_meta( $user->ID, 'last_name', true ),
					'title'      => get_user_meta( $user->ID, 'title', true ),
					'cookie'     => $cookie,
				) );

				return $response;

			} else {

				$response->set_status( 401 );

				return $response;

			}
		}
		$response->set_status( 401 );

		return $response;
	}


	// Edit user
	/**
	 * @param $data
	 *
	 * @return WP_JSON_Response
	 */
	public function edit_user( $data ) {
		$response = ( defined( 'JSON_API_VERSION' ) ) ? new WP_JSON_Response(): new WP_REST_Response;

		// Get current user
		global $brc_user;

		// Authentic user
		if ( ! $this->is_user_logged_in() ) {
			$response->set_status( 401 );

			return $response;
		}

		// Setup values
		$email      = sanitize_text_field( $data['email'] );
		$password   = sanitize_text_field( $data['password'] );
		$first_name = sanitize_text_field( $data['first_name'] );
		$last_name  = sanitize_text_field( $data['last_name'] );
		$title      = sanitize_text_field( $data['title'] );

		// Setup validation errors array
		$errors = array();

		// Validate first name
		$errors = $this->validate_first_name( $first_name, $errors );

		// Validate last name
		$errors = $this->validate_last_name( $last_name, $errors );

		// Validate email
		$errors = $this->validate_email( $email, $errors );

		if ( ! empty( $password ) ) {
			// Validate password
			$errors = $this->validate_password( $password, $errors );
		}
		// Check for any validation errors
		if ( count( $errors ) >= 1 ) {
			$response->set_status( 400 );
			$response->set_data( $errors );

			return $response;
		}

		// Check for password and add to data
		if ( ! empty( $password ) ) {
			wp_set_password( $password, intval( $brc_user['ID'] ) );
		}


		// Merge into old user data
		if ( ! empty( $email ) ) {
			$email_exists = email_exists( $email );
			if ( false === $email_exists || intval( $brc_user['ID'] ) === intval( $email_exists ) ) {
				$user_data = get_user_by( 'id', intval( $brc_user['ID'] ) );

				$user_data->user_email = $email;
				// Update user
				wp_update_user( $user_data );
			} else {
				$errors[] = 'email_exists';
			}
		}

		update_user_meta( intval( $brc_user['ID'] ), 'first_name', $first_name );
		update_user_meta( intval( $brc_user['ID'] ), 'last_name', $last_name );
		update_user_meta( intval( $brc_user['ID'] ), 'title', $title );

		if ( count( $errors ) >= 1 ) {
			$response->set_status( 400 );
			$response->set_data( $errors );

			return $response;
		}

		$user = get_user_by( 'ID', $brc_user['ID'] );

		$this->set_authentication_cookies( $user );
		$cookie = $this->set_auth_cookie( $user );
		$response->set_status( 200 );
		$response->set_data( array(
			'ID'         => $brc_user['ID'],
			'email'      => $email,
			'first_name' => $first_name,
			'last_name'  => $last_name,
			'title'      => $title,
			'cookie'     => $cookie,
		) );
		$response->set_headers( array( 'Cache-Control' => 'no-cache, must-revalidate, max-age=0 ' ) );

		return $response;
	}

	// Logout user
	/**
	 * @return WP_JSON_Response
	 */
	public function logout() {
		wp_clear_auth_cookie();
		$response = ( defined( 'JSON_API_VERSION' ) ) ? new WP_JSON_Response(): new WP_REST_Response;
		$response->set_status( 200 );
		$response->set_data( array( 'message' => 'success' ) );

		return $response;
	}

	// Set cookie information and log in user
	/**
	 * @param $user
	 */
	public function set_authentication_cookies( $user ) {
		wp_set_current_user( $user->ID, $user->user_login );
		wp_set_auth_cookie( $user->ID );
		do_action( 'wp_login', $user->user_login, $user );
	}

	// Forgot password
	/**
	 * @param $data
	 *
	 * @return WP_JSON_Response
	 */
	public function forgot_password( $data ) {

		$response = ( defined( 'JSON_API_VERSION' ) ) ? new WP_JSON_Response(): new WP_REST_Response;
		$email     = sanitize_email( $data['email'] );
		$recaptcha = sanitize_text_field( $data['g-recaptcha-response'] );
		$errors    = array();

		// Check to ensure user isn't logged in
		if ( $this->is_user_logged_in() ) {
			$response->set_status( 409 );
			$response->set_data( array( 'message' => 'user_logged_in' ) );

			return $response;
		}

		// Validate email
		if ( ! filter_var( $email, FILTER_VALIDATE_EMAIL ) || empty( $email ) || strlen( $email ) < 1 ) {
			$errors[] = 'email';
		}

		// Validate captcha
		if ( empty( $recaptcha ) || ! $this->validate_request( $recaptcha ) ) {
			$errors[] = 'recaptcha';
		}

		// Check for any validation errors
		if ( count( $errors ) >= 1 ) {
			$response->set_status( 400 );
			$response->set_data( $errors );

			return $response;
		}

		// Check if user exists
		if ( false === email_exists( $email ) ) {
			$response->set_status( 409 );
			$response->set_data( array( 'message' => 'email_not_found' ) );

			return $response;
		}

		// Get user information
		$user = get_user_by( 'email', $email );

		// Generate a new password token
		$token = bin2hex( random_bytes( 32 ) );

		// Store token
		update_user_meta( $user->ID, 'token', hash( 'sha256', $token ) );

		// Send email with token and reset url

		$url = get_site_url() . '/register/reset/' . $token;

		$first_name = get_user_meta( $user->ID, 'first_name', true );

		$body = Email_Templates::password_reset( $first_name, $url );

		$success = brc_mail( $user->display_name . '<' . $user->user_email . '>', __( 'Your Facebook Brand Resource Center Account', 'fbb' ), $body );

		if ( $success ) {
			$response->set_status( 200 );
			$response->set_data( array( 'message' => 'success' ) );
		} else {
			$response->set_status( 400 );
			$response->set_data( array( 'message' => 'email_send_failed' ) );
		}

		return $response;
	}

	// Reset password
	/**
	 * @param $data
	 *
	 * @return WP_JSON_Response
	 */
	public function reset_password( $data ) {

		$response = ( defined( 'JSON_API_VERSION' ) ) ? new WP_JSON_Response(): new WP_REST_Response;
		$email    = sanitize_email( $data['email'] );
		$password = sanitize_text_field( $data['password'] );
		$token    = sanitize_text_field( $data['token'] );
		$errors   = array();

		// Validate password
		$errors = $this->validate_password( $password, $errors );

		// Validate token
		if ( empty( $token ) || strlen( $token ) < 1 ) {
			$errors[] = 'token';
		}

		// Check for any validation errors
		if ( count( $errors ) >= 1 ) {
			$response->set_status( 400 );
			$response->set_data( $errors );

			return $response;
		}

		// Check if user exists
		if ( ! email_exists( $email ) ) {
			$response->set_status( 409 );
			$response->set_data( array( 'message' => 'email_not_found' ) );

			return $response;
		}

		// Get user information
		$user = get_user_by( 'email', $email );

		// Get generated token
		$generated = get_user_meta( $user->ID, 'token', true );

		// Hash provided token
		$token = hash( 'sha256', $token );

		// Check if generated token and provided match
		if ( $token !== $generated ) {
			$response->set_status( 400 );
			$response->set_data( array( 'message' => 'token_mismatch' ) );

			return $response;
		}

		// Set password
		wp_set_password( $password, $user->ID );

		// Reset token
		update_user_meta( $user->ID, 'token', '' );

		// Login user and return information
		$this->set_authentication_cookies( $user );
		$cookie = $this->set_auth_cookie( $user );
		$response->set_status( 200 );
		$response->set_data( array(
			'ID'         => $user->ID,
			'email'      => $user->user_email,
			'first_name' => get_user_meta( $user->ID, 'first_name', true ),
			'last_name'  => get_user_meta( $user->ID, 'last_name', true ),
			'title'      => get_user_meta( $user->ID, 'title', true ),
			'company'    => get_user_meta( $user->ID, 'company', true ),
			'fb'         => ! empty( get_user_meta( $user->ID, 'fb_id' ) ),
			'cookie'     => $cookie,
		) );

		return $response;

	}

	/**
	 * @param $user
	 *
	 * @return string
	 */
	private function set_auth_cookie( $user ) {
		$expiration = HOUR_IN_SECONDS * 3;
		$cookie     = wp_generate_auth_cookie( $user->ID, $expiration );

		return $cookie;
	}

	/**
	 * @param $password
	 * @param $errors
	 *
	 * @return array
	 */
	private function validate_password( $password, $errors ) {
		if ( empty( $password ) || strlen( $password ) < 5 ) {
			$errors[] = 'password';
		}

		$uppercase = preg_match( '/[A-Z]/', $password );
		$lowercase = preg_match( '/[a-z]/', $password );
		$number    = preg_match( '/[0-9]/', $password );
		$special   = preg_match( '/[!@#$%^&*()\-_=+{};:,<.>]/', $password );

		if ( ! empty( $password ) && ! ( 2 < $uppercase + $lowercase + $number + $special )
		) {
			$errors[] = 'password_complexity';
		}

		return $errors;
	}

	/**
	 * @param $email
	 * @param $errors
	 *
	 * @return array
	 */
	private function validate_email( $email, $errors ) {
		if ( ! filter_var( $email, FILTER_VALIDATE_EMAIL ) || empty( $email ) || strlen( $email ) < 1 ) {
			$errors[] = 'email';
		}

		return $errors;
	}

	/**
	 * @param $last_name
	 * @param $errors
	 *
	 * @return array
	 */
	private function validate_last_name( $last_name, $errors ) {
		if ( ! preg_match( '/^[a-zA-Z ]*$/', $last_name ) || empty( $last_name ) || strlen( $last_name ) < 1 ) {
			$errors[] = 'last_name';

			return $errors;
		}

		return $errors;
	}

	/**
	 * @param $first_name
	 * @param $errors
	 *
	 * @return array
	 */
	private function validate_first_name( $first_name, $errors ) {
		if ( ! preg_match( '/^[a-zA-Z ]*$/', $first_name ) || empty( $first_name ) || strlen( $first_name ) < 1 ) {
			$errors[] = 'first_name';

			return $errors;
		}

		return $errors;
	}

	/**
	 * @param $url
	 *
	 * @return array|string|WP_Error
	 */
	private function get_remote_content( $url ) {
		return ( function_exists( 'vip_safe_wp_remote_get' ) ) ? vip_safe_wp_remote_get( esc_url_raw( $url ) ) : wp_remote_get( esc_url_raw( $url ) );
	}
}
