<?php

/**
 *
 */
define( 'VIP_MAINTENANCE_MODE', true );

// Removes jetpack css
add_filter( 'jetpack_implode_frontend_css', '__return_false' );
add_filter( 'show_admin_bar', '__return_false' );
add_action( 'after_setup_theme', 'api_theme_setup' );

// disable WP from guesing URL
remove_filter( 'template_redirect', 'redirect_canonical' );

/**
 *
 */
function api_theme_setup() {
	load_theme_textdomain( 'api-demo', get_template_directory() . '/languages' );
}

// Include custom resources

require_once( __DIR__ . '/custom-post-types.php' );
require_once( __DIR__ . '/custom-fields.php' );

// Include custom API
require_once( __DIR__ . '/api/base.php' );

require_once( __DIR__ . '/api/books.php' );
require_once( __DIR__ . '/api/users.php' );

// Add custom admin scripts
add_action( 'admin_print_styles', 'api_admin_enqueue_styles' );
function api_admin_enqueue_styles() {
	wp_enqueue_style( 'api-admin-styles', get_template_directory_uri() . '/assets/styles/admin.css' );
}

// Add scripts  to support API page
add_action( 'wp_enqueue_scripts', 'api_enqueue_scripts' );
function api_enqueue_scripts() {
	$hume_url = ( function_exists( 'pll_home_url' ) ) ? pll_home_url() : home_url();
	$config = array(
		'ajax_url'           => admin_url( 'admin-ajax.php' ),
		'nonce'              => ( defined( 'JSON_API_VERSION' ) ) ? wp_create_nonce( 'wp_json' ) : wp_create_nonce( 'wp_rest' ),
		'api'                => '/wp-json/',
		'theme_directory'    => untrailingslashit( $hume_url ) . wp_make_link_relative( get_template_directory_uri() ),
		'api_path'           => ( defined( 'JSON_API_VERSION' ) ) ? '/wp-json' : '/wp-json/api-demo/v1',
	);
	// Add custom admin scripts if user is logged in
	wp_enqueue_script( 'api-jquery-ui', '//code.jquery.com/ui/1.11.4/jquery-ui.js', array( 'jquery' ) );
	wp_enqueue_script( 'api-scripts', get_template_directory_uri() . '/assets/scripts/api.js', array( 'api-jquery-ui' ) );
	wp_localize_script( 'api-scripts', 'api_demo_config', $config );
	wp_enqueue_style( 'api-styles', get_template_directory_uri() . '/assets/styles/api.css' );
}



// Initialize API endpoints v1
add_action( 'wp_json_server_before_serve', 'api_init' );
// for v2
add_action( 'init', 'api_init' );
/**
 *
 */
function api_init() {

	global $api_demo;
	$api_demo = array(
		'books'             => new Api_Books(),
	);

	foreach ( $api_demo as $api ) {
		if( defined( 'JSON_API_VERSION' ) ){
			add_filter( 'json_endpoints', array( $api, 'register_routes_v1' ) );
		} else {
			add_filter( 'rest_api_init', array( $api, 'register_routes_v2' ) );
		}

	}
}



// Add SVG mime-type support
add_filter( 'upload_mimes', 'api_mime_types' );
/**
 * @param $mimes
 *
 * @return mixed
 */
function api_mime_types( $mimes ) {
	$mimes['svg'] = 'image/svg+xml';

	return $mimes;
}


// reduce large numbers to human thousand / million or metric K / M
/**
 * @param $num
 * @param int $places
 * @param string $type
 *
 * @return int|string
 */
function api_human_number( $num, $places = 2, $type = 'human' ) {
	if ( '' === trim( $num ) ) {

		return $num;
	}
	$num = intval( $num );
	if ( 'metric' == $type ) {
		$k = 'K';
		$m = 'M';
	} else {
		$k = ' ' . __( 'thousand', 'wp-vote' );
		$m = ' ' . __( 'million', 'wp-vote' );
	}
	if ( $num < 1000 ) {
		$num_format = number_format( $num );
	} else if ( $num < 1000000 ) {
		$num_format = number_format( $num / 1000, $places ) . $k;
	} else {
		$num_format = number_format( $num / 1000000, $places ) . $m;
	}

	return $num_format;
}



/**
 * return a relative if calling a local URL
 *
 * @param $url
 *
 * @return string
 */
function api_make_link_relative_if_local( $url ) {
	if ( - 1 < strpos( $url, get_option( 'siteurl' ) ) ) {

		return wp_make_link_relative( $url );
	}

	return $url;
}
