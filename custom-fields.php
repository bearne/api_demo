<?php

add_action( 'fm_post_book', 'api_define_author' );
add_action( 'fm_post_book', 'api_define_cover' );
add_action( 'fm_post_book', 'api_define_review_repeater' );
add_action( 'fm_post_book', 'api_define_av_rating' );
add_action( 'fm_post_book', 'api_define_isbn' );
/**
 *
 */
function api_define_author() {
	$fm = new Fieldmanager_Select( array(
		'name'       => 'Author',
		'datasource' => new Fieldmanager_Datasource_Post( array( 'query_args' => array( 'post_type' => 'author' ) ) ),
	) );
	$fm->add_meta_box( 'Author', array( 'book' ), 'side', 'default' );
}

/**
 *
 */
function api_define_cover() {
	$fm = new Fieldmanager_Media( array(
		'name'               => 'cover_image',
		'button_label'       => 'Add Image',
		'modal_title'        => 'Select Cover',
		'modal_button_label' => 'Use as Cover Image',
		'preview_size'       => 'medium',
	) );
	$fm->add_meta_box( 'Cover', array( 'book' ), 'normal', 'default' );
}

/**
 *
 */
function api_define_review_repeater() {
	$fm = new Fieldmanager_Group( array(
		'name'              => 'reviews',
		'add_more_label'    => esc_html__( 'Add Review', 'api-book' ),
		'add_more_position' => 'bottom',
		'limit'             => 0,
		'sortable'          => true,
		'children'          =>
			array(
				'rating' => new Fieldmanager_Radios(
					array(
						'name'         => 'rating',
						'inline_label' => true,
						'label'        => __( 'Rating', 'api-book' ),
						'options'      => array(
							'1' => '1',
							'2' => '2',
							'3' => '3',
							'4' => '4',
							'5' => '5',
						),
					)
				),
				'review' => new Fieldmanager_RichTextArea(
					array(
						'name'            => 'review',
						'label'           => __( 'Review', 'api-demo' ),
						'editor_settings' => array(
							'media_buttons' => false,
						),
					)
				),
			),
	) );
	$fm->add_meta_box( __( 'Reviews', 'api-book' ), array( 'book' ), 'normal', 'low' );
}

/**
 *
 */
function api_define_av_rating() {
	$fm = new Fieldmanager_TextField( array(
		'name'       => 'av_rating',
	) );
	$fm->add_meta_box( __( 'Av Rating', 'api-demo' ), array( 'book' ), 'side', 'default' );
}

/**
 *
 */
function api_define_isbn() {
	$fm = new Fieldmanager_TextField( array(
		'name'       => 'isbn',
	) );
	$fm->add_meta_box( __( 'ISBN', 'api-demo' ), array( 'book' ), 'side', 'default' );
}