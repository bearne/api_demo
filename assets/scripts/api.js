jQuery.noConflict();

jQuery(function ($) {
    // -----------------------------------
    // DEMO API
    // -----------------------------------

    function readFile(url, file) {
        var reader = new FileReader();
        reader.onload = function (e) {
            setQuery(url, '{ "file": "' + reader.result + '" }', 'POST', '');
            makeRequest();
        }
        reader.readAsText(file);
    }

    function setQuery(url, data, type, result) {
        $('.home .result input[name="url"]').val(url);
        $('.home .result textarea[name="data"]').val(data);
        $('.home .result span').text(type);
        $('.home .result pre ').html(result);
    }

    function makeRequest(file) {
        if (file) {
            setQuery(api_demo_config.api_path + '/file-upload/', '{}', 'POST', '');
        }
        var url = $('.home .result input[name="url"]').val();
        var data = $('.home .result textarea[name="data"]').val() || '{}';
        var type = $('.home .result span').text() || 'GET';
        if (0 <= type.indexOf('GET')) {
            type = 'GET';
        } else {
            type = 'POST';
        }

        data = ( file ) ? file : JSON.parse(data);
        var $res = $.ajax({
            url: window.location.origin + url,
            dataType: 'json',
            enctype: 'multipart/form-data',
            type: type,
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', api_demo_config.nonce);
            }
        })
            .always(function (res) {
                $('.result pre ').html(JSON.stringify(res, null, 2));
            });
    }

    $('.home .clear').on('click', function (e) {
        e.preventDefault();
        setQuery('', '', 'GET', '');
    });

    $('.home .submit').on('click', function (e) {
        e.preventDefault();
        makeRequest();
    });

    $('.home .endpoint').each(function () {
        var $this = $(this);
        var url = $this.find('code').text();
        var data = $this.find('pre').text();
        var type = $this.find('span').text();

        $this.find('input[name="nonce"]').val(api_demo_config.nonce);
        $this.find('code').text(api_demo_config.api_path + url);

        var $top = $('<a href="#" class="back">Back to Top</a>').on('click', function (e) {
            e.preventDefault();
            $('html, body').animate({scrollTop: 0}, 300);
        });
        $this.append($top);

        $('[class^=test-]', this).on('click', function (e) {
            e.preventDefault();
            var $parent = $(this).parents('.endpoint ');
            var $example = $parent.find('.example');
            // Get endpoint data
            var url = $parent.find('code').text();
            var data = $parent.find('pre').text();
            var $file = $example.find('input[type="file"]');
            // API is a file upload
            if ($file.length >= 1) {
                var data = new FormData($example.find('find')[0]);
                makerequest(data);
                return;
            }
            // API has example
            if ($example.length >= 1) {
                $example.find('input, textarea').each(function () {
                    var name = $(this).attr('name');
                    input_type = $(this).prop("type").toLowerCase();
                    var val = null;
                    switch (input_type) {
                        case 'text':
                        case 'textarea':
                            val = $(this).val();
                            break;
                        case 'radio':
                        case 'checkbox':
                            if ($(this).attr("checked")) {
                                val = $(this).val();
                            }
                            break
                    }
                    if ( null !== val ) {
                        data = data.replace('{' + name + '}', val);
                        url = url.replace('{' + name + '}', val);
                    }

                });
            }
            url = url.replace('{id}', 1);
            var lang = $('select#lang').val();
            data = data.replace('{en}', lang);
            setQuery( url, data || '{ }', type, '');
            makeRequest();
        });
    });

    $('.home .toc a').on('click', function (e) {
        e.preventDefault();
        var id = $(this).attr('href').replace('hit-', '');
        var top = $(id).offset().top - 60;
        $('html, body').animate({scrollTop: top}, 300);
    });

});